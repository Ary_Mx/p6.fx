﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Animator))]
public class CharacterAnimBasedMovement : MonoBehaviour
{
    public float rotationSpeed = 4f;
    public float rotationThreshold = 0.3f;

    [Header("Animator Parameters")]
    public string motionParam = "motion";

    [Header("Animation Smoothing")]
    [Range(0, 1f)]
    public float StartAnimTime = 0.3f;
    [Range(0, 1f)]
    public float StopAnimTime = 0.15f;

    private float Speed;
    private Vector3 desiredMoveDirection;
    private CharacterController characterController;
    private Animator animator;

    public string mirrorIdleParam;
    private bool mirrorIdle;
    // Start is called before the first frame update
    public float degreesToTurn;
    public string turn180Param;
    private bool turn180;
    void Start()
    {
        characterController = GetComponent<CharacterController>();
        animator = GetComponent<Animator>(); 
    }
    private void OnAnimatorIK(int layerIndex)
    {
       
        if (Speed < rotationThreshold) return;
        float distanceToLeftFoot = Vector3.Distance(transform.position, animator.GetIKPosition(AvatarIKGoal.LeftFoot));
        float distanceToRightFoot = Vector3.Distance(transform.position, animator.GetIKPosition(AvatarIKGoal.RightFoot));

        if(distanceToRightFoot> distanceToLeftFoot)
        {
            mirrorIdle = true;
        }
        else
        {
            mirrorIdle = false;
        }
    }

    public void moveCharacter(float hInput,float vInput,Camera cam,bool jump,bool dash,bool interaction_wall)
    {
        print(vInput);
        // collision con la pared, usar valores negativos
        if (interaction_wall)
        {
            if (vInput > 0)
            {
                vInput = 0.0f;
            }
           
            // hInput = 0.0f;
            dash = false;
        }
        //Calculates the input magnitude
        Speed = new Vector2(hInput,vInput).normalized.sqrMagnitude;
        // Ary: cuando no esta en el suelo

        
        if (jump&&characterController.isGrounded)
        {
            animator.SetBool("jump",true);
            animator.SetFloat("yspeed",1f);
        }
      
        else {
            animator.SetBool("jump",false);
            animator.SetFloat("yspeed",0f);
            animator.SetBool("ground",characterController.isGrounded);
        }





        //Dash only if character has reached maxSpeed (animator parameter value)
        if (Speed>=Speed-rotationThreshold&& dash)
        {
            Speed = 1.5f;
        }

        if(Speed > rotationThreshold)
        {
            animator.SetFloat(motionParam, Speed, StartAnimTime, Time.deltaTime);
            Vector3 forward = cam.transform.forward;
            Vector3 right = cam.transform.right;

            forward.y = 0f;
            right.y = 0f;

            forward.Normalize();
            right.Normalize();
           
            desiredMoveDirection = forward * vInput + right * hInput;
           
            if (Vector3.Angle(transform.forward, desiredMoveDirection) >= degreesToTurn)
                {
                     turn180 = true;
                }
               if (Vector3.Angle(transform.forward, desiredMoveDirection) < degreesToTurn)
            {
                   turn180 = false;
                 transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(desiredMoveDirection), rotationSpeed * Time.deltaTime);

              }
                animator.SetBool(turn180Param, turn180);
               animator.SetFloat(motionParam,Speed, StopAnimTime, Time.deltaTime);
        }

        else if (Speed< rotationThreshold)
          {
              animator.SetBool(mirrorIdleParam,mirrorIdle);
          animator.SetFloat(motionParam, Speed, StopAnimTime, Time.deltaTime);
            }
    }

  
    void Update()
    {
        
    }
}
