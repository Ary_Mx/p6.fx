﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Playables;

[RequireComponent(typeof(Collider))]
public class TimelineActivator : MonoBehaviour
{
    public PlayableDirector playableDirector; //Reference to the PlayableDirector 
    public string playerTAG;                  //Tag to check against when some object enter or exit trigger
    public Transform interactionLocation;     //Reference to the transform where the player will be placed will be placed when the cinematic starts
    public bool autoActivate = false;         //Will be the cinematic activated when an object with a valid TAG enters the trigger.


    public bool interact { get; set; }        // Property activated from outside this object that behaves like an input button.

    [Header("Activation Zone Events")]
    public UnityEvent OnPlayerEnter;          // Events to raise when an object with a valid Tag enters the trigger.
    public UnityEvent OnPlayerExit;           // Events to raise when an object with a valid Tag exits the trigger.

    [Header("Timeline Events")]
    public UnityEvent OnTimeLineStart;         // Event to raise when the Timeline starts playing.
    public UnityEvent OnTimeLineEnd;           // Events to raise when the Timeline stops playing.

    private bool isPlaying;                   // Determine if the Timeline is currently playing 
    private bool playerInside;                //Determines if the player is inside of the player is inside of the trigger or not
    private Transform playerTransform;        // Reference to the player Transform
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    private void Update()
    {
        if(playerInside && !isPlaying)
        {
            if(interact || autoActivate)
            {
                PlayTimeline();
            }
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals(playerTAG))
        {
            playerInside = true;
            playerTransform = other.transform;
            OnPlayerEnter.Invoke();
        }
    }
    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag.Equals(playerTAG)) {
            playerInside = false;
            playerTransform = null;
            OnPlayerExit.Invoke();
        }
    }
    private void PlayTimeline()
    {
        //Place the character at the correct interaction position 
        if (playerTransform && interactionLocation)
            playerTransform.SetPositionAndRotation(interactionLocation.position, interactionLocation.rotation);
        // Avoid infinite interaction loop 
        if (autoActivate)
            playerInside = false;
        //Play de Timeline
        if (playableDirector)
            playableDirector.Play();

        //Set Variables
        isPlaying = true;
        interact = false;
        //Wait for Timeline to end
        StartCoroutine(waitForTimeLineToEnd());
    }
    private IEnumerator waitForTimeLineToEnd()
    {
        //Invoke the methods linked to 
        //the beginning of the cinematic
        OnTimeLineStart.Invoke();
        //Get the duration of the timeline from the playable Director 
        float timeLineDuration = ((float)playableDirector.duration);
        // Wait until the cinematic playing is over
        while (timeLineDuration > 0)
        {
            timeLineDuration -= Time.deltaTime;
            yield return null;
        }
        //Reset variable
        isPlaying = false;

        //Invoke the methods linked to 
        //the end of the cinematic
        OnTimeLineEnd.Invoke();
    }
    
}

